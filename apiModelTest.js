var mongoose = require('mongoose');



var testSchema = mongoose.Schema({    
    desc : {type:String, required:true},
    numb : {type:Number, required:true}    
});



testSchema.index({numb: 1}, {unique: true});


var Test = module.exports = mongoose.model('test', testSchema);

module.exports.get = function (callback, limit) {
    
    Test.find(callback).limit(limit);
}

