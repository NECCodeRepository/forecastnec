// Import contact model
Auth = require('./apiModel');
Fore = require('./apiModelData');
MktP = require('./apiModelMktProgram');
RuleT = require('./apiModelRulesTypes');
Test = require('./apiModelTest');
ForeEst = require('./apiModelForecastEst');

//var _ = require('lodash');
var stringSimilarity = require('string-similarity');
// Handle index actions
exports.index = function (req, res) {

    Auth.get(function (err, auth) {
        //console.log(auth);
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Auth retrieved successfully",
            data: auth
        });
        //console.log(test);
    });
};

// Handle view user auth info
exports.view = function (req, res) {
    //console.log(req.params.username);
    //Auth.findOne({username: req.params.username}, function (err, auth) {
    Auth.findOne({ username: req.params.username }, function (err, auth) {
        //console.log(res);
        //console.log(auth);
        if (err)
            res.send(err);
        res.json({
            message: 'Auth details loading..',
            data: auth
        });
    });
};

// Handle authentication
exports.auth = function (req, res) {
    //console.log(req.params.username);
    //console.log(req.params.password);
    //Auth.findOne({username: req.params.username}, function (err, auth) {
    Auth.findOne({ username: req.params.username,password:req.params.password }, function (err, auth) {
        //console.log(res);
        //console.log(auth);
        if (err)
            res.send(err);
        //console.log(auth['password']);
        if (auth) {
            if (auth['password'] != req.params.password) {
                res.json({
                    message: 'Verifing identity',
                    data: { status: 'nook' }
                });
            } else {
                res.json({
                    message: 'Verifing identity',
                    data: { status: 'ok', name: auth['fullname'] }
                });
            }
        } else {
            res.json({
                    message: 'Verifing identity',
                    data: { status: 'nook' }
                });
        }
        /*
        res.json({
            message: 'Auth details loading..',
            data: auth
        });
        */
    });
};

// Handle create contact actions
exports.newMktProgram = function (req, res) {
    //console.log("llego");
    var mktp = new MktP();
    mktp.mktprogram = req.body.mktprogramDesc;

    mktp.save(function (err) {
        if (err){
            if (err.code === 11000) {
                // Duplicate username
                res.json({
                    status : 500,
                    message: 'El registro de forecast para el mes seleccionado ya existe.'
                });
            }
        } else {
            res.json({
                status : 200,
                message: 'New mktprogram created!',
                data: mktp
            });
        }
    });

};

// Handle index actions
exports.indexMktProgram = function (req, res) {
    //console.log("llego");
    MktP.get(function (err, mktp) {
        //console.log(fore);
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "MktProgram retrieved successfully",
            data: mktp
        });
        //console.log(test);
    });
};

// Handle create contact actions

// Handle create contact actions
exports.newData = function (req, res) {
    //console.log("llego");
    var fore = new Fore();
    fore.forecastName = req.body.forecastName;
    fore.dateLoad = req.body.dateLoad;
    fore.year = req.body.year;
    fore.month = req.body.month;
    fore.data = req.body.data;
    fore.rows = req.body.rows;
    fore.monthNumber = req.body.monthNumber;
    fore.sheet = req.body.sheet;

    fore.save(function (err) {
        if (err){
            if (err.code === 11000) {
                // Duplicate username
                res.json({
                    status : 500,
                    message: 'El registro de forecast para el mes seleccionado ya existe.'
                });
            }
        } else {
            res.json({
                status : 200,
                message: 'New forecast created!',
                data: fore
            });
        }
    });

};


// Handle index actions
exports.foreyeard = function (req, res) {  
    //console.log("llego a foreyeard")  
    Fore.distinct("year",function (err, fore) {        
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Forecasts year retrieved successfully",
            data: fore
        });
        //console.log(test);
    });
};

// Handle index actions
exports.foremonthd = function (req, res) {  
    //console.log("llego a monthd")  
    Fore.distinct("month",{'year':req.params.year},function (err, fore) {        
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Forecasts month retrieved successfully",
            data: fore
        });
        //console.log(test);
    });
};

exports.deleteMktProgram = function (req, res) {

    MktP.remove({
        _id: req.params.id
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Mktprogram deleted'
        });
    });

};

exports.deleteRuleType = function (req, res) {

    RuleT.remove({
        _id: req.params.id
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Ruletype deleted'
        });
    });

};

exports.newRuleType = function (req, res) {
    //console.log("llego");
    var rulet = new RuleT();
    rulet.ruletype = req.body.selectType;
    rulet.mktprogram = req.body.selectMktPrograms;
    rulet.description = req.body.description;
    rulet.months = req.body.selectMonths;

    rulet.save(function (err) {
        if (err){
            if (err.code === 11000) {
                // Duplicate username
                res.json({
                    status : 500,
                    message: 'El registro de ruletype  ya existe.'
                });
            }
        } else {
            res.json({
                status : 200,
                message: 'New ruletype created!',
                data: rulet
            });
        }
    });

};

// Handle index actions
exports.indexRuleType = function (req, res) {
    //console.log("llego");
    RuleT.get(function (err, rulet) {
        //console.log(fore);
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Rulestypes retrieved successfully",
            data: rulet
        });
        //console.log(test);
    });
};

// Handle index actions
exports.indexData = function (req, res) {
    //console.log("llego");
    Fore.find({sheet:'orders'},function (err, fore) {
        //console.log(fore);
        if (err) {
            console.log(err);
            res.json({
                status: "error",
                message: err,
            });
        }
        //console.log(fore);
        res.json({
            status: "success",
            message: "Forecasts retrieved successfully",
            data: fore
        });
        //console.log(test);
    });
};

exports.indexDataIdS = function (req, res) {
    var resultado;    
    var uno;
    
    Fore.findOne({year:req.params.ano,month:req.params.mes}, function (err, fore) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Forecasts retrieved successfully",
            data: fore
        });
    });
};

// Handle view user auth info
exports.indexDataId = function (req, res) {
    var resultado;    
    var uno;
    var dos;    
    var vista = 'orders';
    //console.log(req.params);
    if (req.params.view == 1 || req.params.view == 2)
        vista='sales';
    //console.log(vista)
    if (req.params.view != 2) {
        Fore.findOne({year:req.params.ano,month:req.params.mes,sheet:vista}, function (err, fore) {
            try {
                uno = fore['data'];            
                //console.log(fore);
            } catch (e) {
                console.log("not JSON");
            }            
                Fore.findOne({year:req.params.anob,month:req.params.mesb,sheet:vista}, function (err, fore) {
                    if (fore) {
                        try {
                            dos = fore['data'];                
                        } catch (e) {
                            console.log("not JSON");
                        }
                        resultado = diff(uno, dos,req.params.view);
                        //console.log("res:" + resultado);

                        res.json({
                            status : "200",
                            message: "ok",
                            data: resultado
                        })
                    } else {
                        res.json({
                            status : "500",
                            message: "No se encuentran los datos del FORECAST anterior."                    
                        })
                    }
                });            
        });
    } else {
        //console.log('entro a la opcion de prediction')
        ForeEst.find({year:req.params.ano,month:req.params.mesforview}, function (err, fet) {
            try {                
                uno = fet;                            
                //console.log(uno)
            } catch (e) {
                console.log("not JSON");
            }
            Fore.findOne({year:req.params.anob,month:req.params.mesb,sheet:vista}, function (err, fore) {
                if (fore) {
                    try {
                        dos = fore['data'];                
                        //console.log(dos)
                    } catch (e) {
                        console.log("not JSON");
                    }
                    resultado = diff(uno, dos,req.params.view);
                    res.json({
                        status : "200",
                        message: "ok",
                        data: resultado
                    })
                } else {
                    res.json({
                        status : "500",
                        message: "No se encuentran los datos del FORECAST anterior."                    
                    })
                }
            });
        });
    }
};

var isEmptyObject = function (obj) {
    var name;
    for (name in obj) {
        return false;
    }
    return true;
};

var diff = function (obj1, obj2,vista) {    
    var result = {};
    var resultNo = {};
    var resultNuevo = {}
    var resultChange = {};
    var contadorNo = 0;
    var resultTotal = {};
    var elementNuevo = {}    ;
    var antiguo = obj2;
    var nuevo = obj1;
    var indiceChange = 0;
    var indiceEliminado=0;
    var indiceContinua=0;
    var indiceNuevo=0;
    var esDistinto= false;
    var sumaNuevo = 0;
    var sumaAnterior = 0;
    var mesNuevo = 0;
    var mesAnterior = 0;
    var mesesArray=['Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre','Enero','Febrero','Marzo'];

   antiguo.forEach(function (element) {            
        nuevo.forEach(function(element2) {            
            if (element['customer'] == element2['customer'] && element['project'] == element2['project']){
                esDistinto=false;
                sumaNuevo = 0;
                sumaAnterior = 0;
                if (vista == 2) {
                    monthsarray2 = element2['months'];
                } else {
                    monthsarray2 = element2['fcMes'];
                }

                for (let i =0; i<= 11; i++){
                    sumaNuevo = element['fcMes'][i] + sumaNuevo;
                    sumaAnterior = element['fcMes'][i] + sumaAnterior;                    
                        if (monthsarray2[i] != element['fcMes'][i]){
                            esDistinto = true;
                            if (monthsarray2[i] < element['fcMes'][i]) {
                                mesAnterior = mesesArray[i];
                            } else if (monthsarray2[i] > element['fcMes'][i]) { 
                                mesNuevo = mesesArray[i];
                            } else {
                                mesNuevo = mesesArray[i];
                                mesAnterior = mesNuevo;
                            }
                            
                        }                    
                }
                if (esDistinto == true) {
                    //element.fcMesNuevo = element2.fcMes;
                    element.fcMesNuevo = monthsarray2;
                    element.sumaNuevo = sumaNuevo;
                    element.sumaAnterior = sumaAnterior;
                    element.mesAnterior = mesAnterior;
                    element.mesNuevo = mesNuevo;
                    resultChange[indiceChange] = element;
                    contadorNo++;
                    indiceChange++;
                } else {
                    var a = element['fcMes'];
                    element.fcMes = a.reduce(function(a, b) { return parseInt(a) + parseInt(b); }, 0);
                    result[indiceContinua] = element;
                    contadorNo++;
                    indiceContinua++;
                }

            }
        })
        if (contadorNo == 0) {
            var a = 0;
            element['fcMes'].forEach(element => {
                a = a + parseInt(element);
            });            
            element.fcMes = a;            
            resultNo[indiceEliminado] = element;
            indiceEliminado++;
        } 
        contadorNo = 0;  
    });
    contadorNo = 0;
    nuevo.forEach(function(element) {
        element.test = 'test';
        //console.log(typeof element);
        elementNuevo = {};
        antiguo.forEach(function (element2) {
            if (element['customer'] == element2['customer'] && element['project'] == element2['project']) {
                contadorNo++;
            }
        });

        if (contadorNo == 0) { 
  
            if (vista != 2) {
                var a = element['fcMes'];
                element.fcMes = a.reduce(function(a, b) { return parseInt(a) + parseInt(b); }, 0);

                resultNuevo[indiceNuevo] = element;
                indiceNuevo++;
            } else {
                var a = element['months'];
                elementNuevo.customer = element.customer;
                elementNuevo.project = element.project;
                elementNuevo.mkt = element.mkt;
                elementNuevo.fcMes = a.reduce(function(a, b) { return parseInt(a) + parseInt(b); }, 0); 

                resultNuevo[indiceNuevo] = elementNuevo;
                indiceNuevo++;
            }            
           
            //resultNuevo[indiceNuevo] = elementNuevo;
            //indiceNuevo++;
        } 
        contadorNo = 0;
    });

    resultTotal.continuan = result;
    resultTotal.eliminados = resultNo;
    resultTotal.cambiados = resultChange;
    resultTotal.nuevos = resultNuevo;
    //console.log(resultTotal);
    return resultTotal;
};

exports.deleteData = function (req, res) {
    Fore.remove({
        _id: req.params.id
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Forecast deleted'
        });
    });
};

exports.deleteDataParams = function (req, res) {
    //console.log("deleted params");
    //console.log(req.params.year);
    //console.log(req.params.month);
    Fore.deleteMany({
        year: req.params.year,
        month: req.params.month    
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Forecast deleted'
        });
    });
};

// Handle create contact actions
exports.new = function (req, res) {
    var auth = new Auth();

    auth.username = req.body.username ? req.body.username : auth.username;
    auth.password = req.body.password;

    // save the contact and check for errors
    auth.save(function (err) {
        /*
        if (err.name === 'MongoError' && err.code === 11000) {
            // Duplicate username
            return res.status(500).send({ succes: false, message: 'User already exist!' });
        }
        */
        // if (err)
        //     res.json(err);
        res.json({
            message: 'New user created!',
            data: auth
        });
    });
};


// Handle delete contact
exports.delete = function (req, res) {


    Test.remove({
        _id: req.params.test_id
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Test deleted'
        });
    });

};

exports.newForecastest = function (req,res) {
    //var fe = new ForeEst();
    //console.log(req.body);
    
    ForeEst.insertMany(req.body).then((docs)=>{
        //console.log(docs);
        res.json({
            message: 'New documents created!',
            data: docs
        });
      });
      
}

// Handle create contact actions
exports.newTest = function (req, res) {
    var test = new Test();
    //console.log("llego sin problemas")  
    //console.log(req.body)
    
    Test.insertMany(req.body).then((docs)=>{
        //console.log(docs);
        res.json({
            message: 'New documents created!',
            data: docs
        });
      });
};

exports.deleteForecastest = function (req, res) {
    ForeEst.remove({
        orderId: req.params.orderId
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Forecastes deleted'
        });
    });
};

exports.deleteForecastestUnique = function (req, res) {
    ForeEst.remove({
        _id: req.params.id
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Forecastes One deleted'
        });
    });
};

exports.updateForecastest  = function (req, res) {
    //console.log(req.params);
    //console.log(req.body);
    ForeEst.findByIdAndUpdate(req.params.id, {
        months: req.body.fe,
        postponeRecomended : req.body.meses,
        monthValueIndex : req.body.indice
    }, {new: true})
    .then(foreest => {
        if(!foreest) {
            return res.status(404).send({
                message: "foreest not found with id " + req.params.id
            });
        }
        res.send(foreest);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "foreest not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.id
        });
    });
}

exports.indexForecastest = function (req, res) {
    //console.log(req.params);
    ForeEst.find({orderId:req.params.id},function (err, fe) {
        //console.log(fore);
        if (err) {
            console.log(err);
            res.json({
                status: "error",
                message: err,
            });
        }
        //console.log(fe);
        res.json({
            status: "success",
            message: "Forecastests retrieved successfully",
            data: fe
        });
        //console.log(test);
    });
};