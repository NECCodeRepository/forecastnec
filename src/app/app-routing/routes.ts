import { Routes } from '@angular/router';

import { AppComponent } from '../app.component';
import { LoginComponent } from '../login/login.component';
import { MainComponent } from '../main/main.component';
import { DataComponent } from '../data/data.component';
import { RulesComponent } from '../rules/rules.component';
import { SalesComponent } from '../sales/sales.component';
import { MktprogramComponent } from '../mktprogram/mktprogram.component';
import { AuthGuard } from '../auth.guard';


export const routes: Routes = [
  { path: 'app',  component: AppComponent },
  { path: 'login',     component: LoginComponent },
  { path: 'main',     component: MainComponent,canActivate: [AuthGuard] },
  { path: 'data',     component: DataComponent,canActivate: [AuthGuard] },
  { path: 'rules',     component: RulesComponent,canActivate: [AuthGuard] },
  { path: 'mktprogram',     component: MktprogramComponent,canActivate: [AuthGuard] },
  { path: 'sales',     component: SalesComponent,canActivate: [AuthGuard] },
  
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];