import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { sha256, sha224 } from 'js-sha256';
import { AuthService } from '../services/auth.service';
import { Auth, AuthClass } from '../shared/auth';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  title = 'forecastNEC';
  loginForm: FormGroup;
  model: AuthClass = { username: "admin", password: "admin" };
  username: string;
  password: string;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(private authService: AuthService, private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    //console.log("llego");
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.authService.logout();
  }

  get f() { return this.loginForm.controls; }

  login() {
    //console.log("llamado a api");
    //console.log(sha256(this.password));
    //console.log(shajs('sha256').update('hola').digest('hex'));
    //console.log(this.username);
    //console.log(sha256(this.password));
    this.authService.getAuth(this.username, sha256(this.password)).subscribe(auth => {
      //console.log(auth['data']['status']);
      
      if (auth['data']['status'] == "ok") {
        localStorage.setItem('isLoggedIn', "true");
        localStorage.setItem('token', this.f.username.value);
        
        localStorage.setItem('fullname',auth['data']['name']);
        this.router.navigate(['/data']);
      } else {
        this.username = "";
        this.password = "";
      }
    });

    //this.router.navigate(['/data']);
  }
}
