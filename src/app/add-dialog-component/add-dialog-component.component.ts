import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { Months } from '../shared/rulesTypes';
import { RulestypesService } from '../services/rulestypes.service';
import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';


registerLocaleData(es);
@Component({
  selector: 'app-add-dialog-component',
  templateUrl: './add-dialog-component.component.html',
  styleUrls: ['./add-dialog-component.component.css']
})
export class AddDialogComponentComponent implements OnInit {
  months: Months[];
  selected: any;
  mktPrograms: string[];
  desabilitado: boolean = false;
  sProject : string;
  sCustomer : string;
  selectedMktp : any;
  sAmount : number;
  
  constructor(public dialogRef: MatDialogRef<AddDialogComponentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private rulestypesService: RulestypesService
  ) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  public inputValidator(event: any) {
    //console.log(event.target.value);
    const pattern = /^[0-9]*$/;   
    //let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/g, "");      
    }
  }

  ngOnInit() {
    this.rulestypesService.getMktPrograms().subscribe(mktprograms => {
      this.mktPrograms = mktprograms['data'];
      //console.log(mktprograms);
    });
    //this.selected = this.data.meses;
    this.months = [
      { value: 10, viewValue: 'FC_JAN' },
      { value: 11, viewValue: 'FC_FEB' },
      { value: 12, viewValue: 'FC_MAR' },
      { value: 1, viewValue: 'FC_APR' },
      { value: 2, viewValue: 'FC_MAY' },
      { value: 3, viewValue: 'FC_JUN' },
      { value: 4, viewValue: 'FC_JUL' },
      { value: 5, viewValue: 'FC_AGO' },
      { value: 6, viewValue: 'FC_SEP' },
      { value: 7, viewValue: 'FC_OCT' },
      { value: 8, viewValue: 'FC_NOV' },
      { value: 9, viewValue: 'FC_DEC' },
    ];
  }

  meses(e) {
    //console.log(e);
    this.data.meses = e;
    //this.cuota = this.data.fcMes / e;
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.data.accion = "save";
    this.data.customer = this.sCustomer;
    this.data.project = this.sProject;
    this.data.mkt = this.selectedMktp;
    this.data.amount = this.sAmount;
    
    this.dialogRef.close({ data: this.data })    
  }

}
