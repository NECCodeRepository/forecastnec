import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import * as $ from 'jquery';
import { RulesTypes, Months } from '../shared/rulesTypes';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { RulestypesService } from '../services/rulestypes.service';
import { RuleType } from '../shared/rulesTypes';
import { NgFlashMessageService } from 'ng-flash-messages';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {
  @ViewChild('fform') registerFormDirective;
  rulesTypes: RulesTypes[];
  months: Months[];
  rulesTypesForm: FormGroup;
  rulestypes: RuleType[];
  ruletype: RuleType;
  mktPrograms: string[];
  dataSource = new MatTableDataSource();
  selected: any;
  modalRef: BsModalRef;
  selectedMktp: any;
  row: Object;
  fullname: string;
  id: string;
  displayedColumns: string[] = ['ruletype', 'mktprogram', 'description', 'months', 'delete'];

  constructor(private rl: FormBuilder,
    private rulestypesService: RulestypesService,
    private ngFlashMessageService: NgFlashMessageService,
    private modalService: BsModalService,
    private router: Router,
    public authService: AuthService) { }

  ngOnInit() {
    this.id = localStorage.getItem('token');
    this.fullname = localStorage.getItem('fullname');
    this.rulestypesService.getMktPrograms().subscribe(mktprograms => {
      this.mktPrograms = mktprograms['data'];
      //console.log(mktprograms);
    });

    this.rulesTypes = [
      { value: 'Mkt Program', viewValue: 'Mkt Program' },
      { value: 'Descripción', viewValue: 'Descripción' },
      { value: 'Mkt & Descripción', viewValue: 'Mkt & Descripción' },
    ];

    this.months = [
      { value: 12, viewValue: '12 Meses' },
      { value: 1, viewValue: '1 Meses' },
      { value: 2, viewValue: '2 Meses' },
      { value: 3, viewValue: '3 Meses' },
      { value: 6, viewValue: '6 Meses' },
      { value: 10, viewValue: '10 Meses' },
    ];
    //this.selectedMktp = "Seleccione";
    this.rulesTypesForm = this.rl.group({
      selectType: ['Mkt Program', Validators.required],
      selectMktPrograms: this.selectedMktp,
      description: '',
      selectMonths: [this.selected,Validators.required]
    });

    this.rulesTypesForm.controls['description'].disable();

    this.refresh();
  }



  logout(): void {
    //console.log("Logout");
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  decline(): void {
    this.modalRef.hide();
  }

  openModal(template: TemplateRef<any>, element: Object) {
    this.row = element;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  refresh() {
    this.rulestypesService.getRulesTypes().subscribe(rulestypes => {
      this.rulestypes = rulestypes
      this.dataSource = new MatTableDataSource(rulestypes['data']);
      console.log(rulestypes['data']);
    });

  }

  ngAfterViewInit() {
    $.getScript("../../build/js/custom.js", function () {
    });
  }

  deleteRecord() {
    //e.stopPropagation();
    this.rulestypesService.deleteRuleType(this.row['_id']).subscribe(forecast => {
      this.refresh();
    });
    this.modalRef.hide();
  }

  selectTypeOnChange(event) {
    if (event == "Descripción") {
      this.rulesTypesForm.controls['description'].enable();
      this.rulesTypesForm.controls['selectMktPrograms'].disable();
    } else if (event == "Mkt Program") {
      this.rulesTypesForm.controls['description'].disable();
      this.rulesTypesForm.controls['selectMktPrograms'].enable();
    } else {
      this.rulesTypesForm.controls['description'].enable();
      this.rulesTypesForm.controls['selectMktPrograms'].enable();
    }
  }

  accion() {
    let valido : boolean = true;
    let validt : boolean = true;
    let validm : boolean = true;
    this.ruletype = this.rulesTypesForm.value;

    if (!this.rulesTypesForm.value.description) {
      this.rulesTypesForm.value.description = 'n/a';
      valido = false;
    }
    if (!this.rulesTypesForm.value.selectMktPrograms) {
      this.rulesTypesForm.value.selectMktPrograms = 'n/a';
      validt = false;
    }
    if (!this.rulesTypesForm.value.selectMonths) {
      validm = false;
    }
    console.log(valido,validt,validm);
    console.log(this.rulesTypesForm.value.selectMonths)
    if (valido == true && validm == true|| validt == true && validm == true || valido == true && validt == true && validm == true) {
      this.rulestypesService.newRuleType(this.ruletype).subscribe(result => {
        //console.log(forecast)
        if (result['status'] == 200) {
          console.log("status 200");
          this.refresh();
          //this.mkprogramDesc.nativeElement.value = '';
          this.rulesTypesForm.reset({
            selectType: ['Mkt Program', Validators.required],
            selectMktPrograms: this.selectedMktp,
            description: '',
            selectMonths: [Validators.required]
          });
          this.registerFormDirective.resetForm();


          this.ngFlashMessageService.showFlashMessage({
            messages: ["Registro de FORECAST guardado con exito!"],
            dismissible: true,
            timeout: 3000,
            // Type of flash message, it defaults to info and success, warning, danger types can also be used
            type: 'success'
          });

        } else {
          console.log("no estatus");

          this.ngFlashMessageService.showFlashMessage({
            messages: [result['message']],
            dismissible: true,
            timeout: 3000,
            // Type of flash message, it defaults to info and success, warning, danger types can also be used
            type: 'danger'
          });

        }
      });
    } else {
      this.ngFlashMessageService.showFlashMessage({
        messages: ["Debe seleccionar una categoria de MKT o descripcion"],
        dismissible: true,
        timeout: 3000,
        // Type of flash message, it defaults to info and success, warning, danger types can also be used
        type: 'danger'
      });
    }


  }

}
