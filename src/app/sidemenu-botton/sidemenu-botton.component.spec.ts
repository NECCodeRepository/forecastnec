import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidemenuBottonComponent } from './sidemenu-botton.component';

describe('SidemenuBottonComponent', () => {
  let component: SidemenuBottonComponent;
  let fixture: ComponentFixture<SidemenuBottonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidemenuBottonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidemenuBottonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
