import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef, AfterViewInit,ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { Years, MonthsFC } from '../shared/years';
import { ForecastService } from '../services/forecast.service';
import { Router } from '@angular/router';
import { Forecast, ForecastClass } from '../shared/forecast';
import { forEach } from '@angular/router/src/utils/collection';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material';
import * as $ from 'jquery'
import { AuthService } from '../services/auth.service';
import { NgFlashMessageService } from 'ng-flash-messages';
import * as XLSX from 'xlsx';

export interface View {
  value: number;
  viewValue: string;
}


@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit, AfterViewInit {
  @ViewChild('fform') registerFormDirective;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('sortCol2') sortCol2: MatSort;
  @ViewChild('TABLE') table: ElementRef;
  registerForm: FormGroup;
  selected: any;
  selectedMonth: any;
  selectedView: any;
  years: string[];
  dataSource = new MatTableDataSource();
  dataSourceEliminados = new MatTableDataSource();
  dataSourceContinuados = new MatTableDataSource();
  dataSourceNuevos = new MatTableDataSource();
  months: string[];
  totalCambiados: any = 0;
  totalEliminados: any = 0;
  totalContinuan: any = 0;
  totalCambiadosNuevos : any = 0;
  totalNuevos: any = 0;
  resumenModificados: any = 0;
  resumenNuevos: any = 0;
  resumenEliminados: any = 0;
  resumenContinuados: any = 0;
  monthsFC: MonthsFC[];
  fullname : string;
  displayedColumns: string[] = ['customer', 'project', 'sbu', 'mkt', 'supplier', 'fcMes'];
  displayedColumnsModificados: string[] = ['customer', 'project', 'sbu', /*'mkt', 'supplier',*/'mesAnterior', 'sumaAnterior','mesNuevo','sumaNuevo'];
  views: View[] = [
    {value: 0, viewValue: 'ORDERS'},
    {value: 1, viewValue: 'SALES'},
    {value: 2, viewValue: 'PREDICTION'},
  ];
  id: string;
  //displayedColumns: string[] = ['customer'];


  constructor(private ngFlashMessageService: NgFlashMessageService,
    private router: Router, 
    private fb: FormBuilder, 
    private forecastService: ForecastService, 
    public authService: AuthService) {
    this.createForm();
  }

  ngAfterViewInit() {
    $.getScript("../../build/js/custom.js", function () {
    });
  }

  ngOnInit() {
    this.id = localStorage.getItem('token');
    this.fullname = localStorage.getItem('fullname');
    this.selectedView = 0;
    this.forecastService.getForeYearD().subscribe(years => {
      this.years = years['data'];
    });
    this.monthsFC = [
      { value: "FC_NOV", viewValue: "NOVIEMBRE", ant: "FC_OCT",num:7 },
      { value: "FC_DEC", viewValue: "DICIEMBRE", ant: "FC_NOV",num:8 },
      { value: "FC_JAN", viewValue: "ENERO", ant: "FC_DEC",num:9 },
      { value: "FC_FEB", viewValue: "FEBRERO", ant: "FC_JAN",num:10 },
      { value: "FC_MAR", viewValue: "MARZO", ant: "FC_FEB", num:11 },
      { value: "FC_APR", viewValue: "ABRIL", ant: "FC_MAR", num:12 },
      { value: "FC_MAY", viewValue: "MAYO", ant: "FC_APR", num:1 },
      { value: "FC_JUN", viewValue: "JUNIO", ant: "FC_MAY",num:2 },
      { value: "FC_JUL", viewValue: "JULIO", ant: "FC_JUN",num:3 },
      { value: "FC_AGO", viewValue: "AGOSTO", ant: "FC_JUL",num:4 },
      { value: "FC_SEP", viewValue: "SEPTIEMBRE", ant: "FC_AGO",num:5 },
      { value: "FC_OCT", viewValue: "OCTUBRE", ant: "FC_SEP",num:6 }
    ];

  }

  ExportTOExcel()
{
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'modificados');
  
  /* save to file */
  XLSX.writeFile(wb, 'modificados.xlsx');
  
}



  logout(): void {
    //console.log("Logout");
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  createForm(): void {
    this.registerForm = this.fb.group({
      selectYears: [this.selected, Validators.required],
      selectMonths: [this.selectedMonth, Validators.required],
      selectView: [0]
    });
  }

  yearChange(e) {
    this.forecastService.getForeMonthD(e).subscribe(months => {
      this.months = months['data'];
    });
    this.selected = e;
  }

  viewChange(){
    this.monthChange(this.selectedMonth);
  }

  monthChange(e) {
    //this.selectedMonth = e;
    let mesanterior: string;
    let anoanterior: number = this.selected;
    let mesforview: number = this.monthsFC[this.monthsFC.findIndex(p => p.ant == e)].num;
    this.totalCambiados = 0;
        this.totalCambiadosNuevos = 0;
        this.totalContinuan = 0;
        this.totalEliminados = 0;
        this.totalNuevos = 0;
    this.monthsFC.forEach(function (element, index) {
      if (element['value'] == e) {
        mesanterior = element['ant'];
      }
    });
    if (e == "FC_JAN") {
      anoanterior = anoanterior - 1;
    }
    
    this.forecastService.getDataAnalysis(this.selected, e, anoanterior, mesanterior,this.selectedView,mesforview).subscribe(data => {
      if (data['status'] == "200") {
        //console.log(data)
        var arreglo = [];
        for (var i = 0; i < Object.keys(data['data']['cambiados']).length; i++) {
          arreglo.push(data['data']['cambiados'][i]);
          //console.log(typeof data['data']['cambiados'][i].fcMesNuevo);
          this.totalCambiados = parseFloat(this.totalCambiados) + parseFloat(data['data']['cambiados'][i].sumaAnterior)
          this.totalCambiadosNuevos = parseFloat(this.totalCambiadosNuevos) + parseFloat(data['data']['cambiados'][i].sumaNuevo)
        }
        //console.log( this.totalCambiadosNuevos);
        if (data['data']['cambiados'])
          this.resumenModificados = Object.keys(data['data']['cambiados']).length;
        this.dataSource = new MatTableDataSource(arreglo);
        this.dataSource.sort = this.sort;
        arreglo = [];
        for (var i = 0; i < Object.keys(data['data']['eliminados']).length; i++) {
          arreglo.push(data['data']['eliminados'][i]);
          this.totalEliminados = parseFloat(this.totalEliminados) + parseFloat(data['data']['eliminados'][i].fcMes)
        }
        if (data['data']['eliminados'])
          this.resumenEliminados = Object.keys(data['data']['eliminados']).length;
        this.dataSourceEliminados = new MatTableDataSource(arreglo);
        this.dataSourceEliminados.sort = this.sort;
        arreglo = [];
        for (var i = 0; i < Object.keys(data['data']['continuan']).length; i++) {
          arreglo.push(data['data']['continuan'][i]);
          this.totalContinuan = parseFloat(this.totalContinuan) + parseFloat(data['data']['continuan'][i].fcMes)          
        }
        if (data['data']['continuan'])
          this.resumenContinuados = Object.keys(data['data']['continuan']).length;
        this.dataSourceContinuados = new MatTableDataSource(arreglo);
        this.dataSourceContinuados.sort = this.sortCol2;
        arreglo = [];
        for (var i = 0; i < Object.keys(data['data']['nuevos']).length; i++) {
          arreglo.push(data['data']['nuevos'][i]);
          this.totalNuevos = parseFloat(this.totalNuevos) + parseFloat(data['data']['nuevos'][i].fcMes)
        }
        if (data['data']['nuevos'])
          this.resumenNuevos = Object.keys(data['data']['nuevos']).length;
        this.dataSourceNuevos = new MatTableDataSource(arreglo);
        this.dataSourceNuevos.sort = this.sort;
      } else {
        //console.log(data)
        this.dataSource = new MatTableDataSource();
        this.dataSourceContinuados = new MatTableDataSource();
        this.dataSourceEliminados = new MatTableDataSource();
        this.dataSourceNuevos = new MatTableDataSource();
        this.totalCambiados = 0;
        this.totalCambiadosNuevos = 0;
        this.totalContinuan = 0;
        this.totalEliminados = 0;
        this.totalNuevos = 0;
        this.ngFlashMessageService.showFlashMessage({
          messages: [data['message']],
          dismissible: false,
          timeout: 10000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
      }
    });


  }

  getTotalNuevos() {
    return this.totalNuevos;
  }

  getTotalContinuan() {
    return this.totalContinuan;
  }

  getTotalContinuanNuevos() {
    return this.totalCambiadosNuevos;
  }

  getTotalEliminados() {
    return this.totalEliminados;
  }

  getTotalCost() {
    return this.totalCambiados;
  }
}
