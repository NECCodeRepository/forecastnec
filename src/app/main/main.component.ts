import { Component, OnInit, ViewChild, Inject, TemplateRef, ChangeDetectorRef, AfterViewInit, ElementRef } from '@angular/core';
import * as XLSX from 'xlsx';
import * as GC from '@grapecity/spread-sheets';
import * as Excel from '@grapecity/spread-excelio';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ForecastService } from '../services/forecast.service';
import { ForecastEstService } from '../services/forecast-est.service';
import { Forecast, ForecastClass } from '../shared/forecast';
import { Years, MonthsFC } from '../shared/years';
import { MatTableDataSource } from '@angular/material/table';
import * as $ from 'jquery';
import { NgFlashMessageService } from 'ng-flash-messages';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

type AOA = any[][];

const monthNames = ["FC_ENE", "FC_FEB", "FC_MAR", "FC_ABR", "FC_MAY", "FC_JUN",
  "FC_JUL", "FC_AGO", "FC_SEP", "FC_OCT", "FC_NOV", "FC_DIC"
];

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, AfterViewInit {
  //public files: UploadFile[] = [];
  modalRef: BsModalRef;
  private spread: GC.Spread.Sheets.Workbook;
  private excelIO;
  tempmap: any; mapped: any; mappedSales: any;
  nromes: number;
  selected: any;
  selectedMonth: any;
  dataSource = new MatTableDataSource();
  envioForescast: Forecast = {};
  envForecast: ForecastClass[];
  result: any;
  buttonText : string = "Save";
  years: Years[];
  progress: number = 0;
  monthsFC: MonthsFC[];
  errMess: string;
  id: string;
  fullname: string;
  disa: boolean = false;
  displayedColumns: string[] = ['year', 'month', 'rows', 'user', 'dateLoad', 'delete'];
  done = [];
  e: any;
  row: Object;
  @ViewChild('fform') registerFormDirective;
  registerForm: FormGroup;
  data: AOA = [[1, 2], [3, 4]];
  @ViewChild('inputFile') myInputVariable: ElementRef;
  //@ViewChild('input') myInputVariable2: ElementRef;

  formErrors = {
    //'forecastName': '',
    'excelInputFile': '',
    'selectYears': '',
    'selectMonths': ''
  };

  validationMessages = {
    'excelInputFile': {
      'required': 'Se requiere un archivo en formato xlsx o xlsb'
    },
    'selectYears': {
      'required': 'Tel. number is required.'
    },
    'selectMonths': {
      'required': 'Email is required.'
    },
  };

  constructor(private router: Router, public authService: AuthService,
    private modalService: BsModalService,
    private ngFlashMessageService: NgFlashMessageService, private fb: FormBuilder,
    private forecastService: ForecastService, @Inject('BaseURL') private BaseURL,
    private changeDetectorRefs: ChangeDetectorRef,
    private forecastEstService: ForecastEstService) {
    this.excelIO = new Excel.IO();
    //this.createForm();
  }

  openModal(template: TemplateRef<any>, element: Object) {
    this.row = element;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  logout(): void {
    //console.log("Logout");
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  decline(): void {
    this.modalRef.hide();
  }

  noone(): Boolean {
    if (this.mapped) {
      return false;
    } else {
      return true;
    }

  }

  accion() {
    //console.log(this.registerForm.value);
    this.envioForescast.forecastName = "prototype"; //this.registerForm.value['forecastName'];
    this.envioForescast.dateLoad = new Date().toISOString();
    this.envioForescast.rows = this.mapped.length;
    this.envioForescast.data = this.mapped;
    this.envioForescast.year = this.selected;
    this.envioForescast.month = this.selectedMonth;
    this.envioForescast.monthNumber = this.nromes;
    this.envioForescast.sheet = 'orders';

    this.forecastService.submitForecast(this.envioForescast).subscribe(orders => {
      //console.log(forecast)
      if (orders['status'] == 200) {
        //console.log(orders);
        //console.log("status 200");
        //console.log(orders['data']['_id'])
        this.envioForescast.rows = this.mappedSales.length;
        this.envioForescast.data = this.mappedSales;
        this.envioForescast.sheet = 'sales'

        this.forecastService.submitForecast(this.envioForescast).subscribe(sales => {
          if (sales['status'] == 200) {
            this.mapped = [];
            this.myInputVariable.nativeElement.value = '';
            this.refresh();
            this.FlashMessageService('Registro de FORECAST guardado con exito!', 3000, 'success');
          } else {
            // delete orders
            //console.log("delete");
            //console.log(sales['message']);
            this.forecastService.deleteForecasts(orders['data']['_id']).subscribe(forecast => {
              this.refresh();
            });
          }
        });

      } else {
        this.FlashMessageService(orders['message'], 3000, 'danger');
      }
    });
  }

  FlashMessageService(message, time, tipo) {
    this.ngFlashMessageService.showFlashMessage({
      messages: [message],
      dismissible: true,
      timeout: time,
      type: tipo
    });
  }

  createForm(): void {
    this.registerForm = this.fb.group({      
      excelInputFile: [null, Validators.required],
      selectYears: [this.selected, Validators.required],
      selectMonths: [this.selectedMonth, Validators.required]
    });

    this.registerForm.valueChanges.subscribe(data => this.onValueChanges(data));

    this.onValueChanges();
  }

  onValueChanges(data?: any) {
    if (!this.registerForm) { return; }
    const form = this.registerForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  ngAfterViewInit() {
    $.getScript("../../build/js/custom.js", function () {
    });
  }

  ngOnInit() {
    this.id = localStorage.getItem('token');
    this.fullname = localStorage.getItem('fullname');
    var d = new Date();
    var n = d.getFullYear();
    this.selected = n;
    this.years = [
      { value: n, viewValue: n },
      { value: n - 1, viewValue: n - 1 }
    ]
    this.selectedMonth = monthNames[d.getMonth()];

    this.monthsFC = [
      { value: "FC_NOV", viewValue: "NOVIEMBRE", ant: "FC_DEC", num: 8 },
      { value: "FC_DEC", viewValue: "DICIEMBRE", ant: "FC_JAN", num: 9 },
      { value: "FC_JAN", viewValue: "ENERO", ant: "FC_FEB", num: 10 },
      { value: "FC_FEB", viewValue: "FEBRERO", ant: "FC_MAR", num: 11 },
      { value: "FC_MAR", viewValue: "MARZO", ant: "FC_APR", num: 12 },
      { value: "FC_APR", viewValue: "ABRIL", ant: "FC_MAY", num: 1 },
      { value: "FC_MAY", viewValue: "MAYO", ant: "FC_JUN", num: 2 },
      { value: "FC_JUN", viewValue: "JUNIO", ant: "FC_JUL", num: 3 },
      { value: "FC_JUL", viewValue: "JULIO", ant: "FC_AGO", num: 4 },
      { value: "FC_AGO", viewValue: "AGOSTO", ant: "FC_SEP", num: 5 },
      { value: "FC_SEP", viewValue: "SEPTIEMBRE", ant: "FC_OcT", num: 6 },
      { value: "FC_OCT", viewValue: "OCTUBRE", ant: "FC_NOV", num: 7 }
    ]
    this.createForm();
    this.refresh();
  }

  refresh() {
    this.forecastService.getForecasts().subscribe(forecasts => {
      this.envForecast = forecasts;
      this.dataSource = new MatTableDataSource(forecasts['data']);
      //console.log(forecasts['data']);
    });
  }

  deleteRecord() {

    this.forecastService.deleteForecastsParams(this.row['year'], this.row['month']).subscribe(forecast => {
      this.refresh();
      this.forecastEstService.deleteForecaststest(this.row['_id']).subscribe(fe => {
        //console.log('borrado fe correctamente');
      });
      //console.log(forecast);
    });
    this.modalRef.hide();
  }

  fileToUpload: File = null;
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onFileChange(evt) {
    this.readExcel(evt, 'OR_List', 'ORDER FORECAST GPM', this.test);
    this.readExcel(evt, 'SL_List', 'SALES FORECAST GPM', this.test);
  }

  test(data, esto,finditem,sheetname) {
    //console.log(data)
    let that = esto;
    //console.log(that);
    
    function Valores() {
      this.valor = 0;
      this.mes = 0;
    }
    var p = new Valores();
    let mesposterior: string;
    that.monthsFC.forEach(function (element, index) {
      if (element['value'] == that.selectedMonth) {
        mesposterior = element['ant'];
        that.nromes = element['num'];
      }
    }.bind(that));
    p.mes = that.data[3].indexOf(mesposterior);

    if (p.mes) {
      let temp: number;
      temp = 12 - that.nromes;
      p.valor = that.data.findIndex(product => product.some(item =>
        item === finditem)
      );
      p.end = that.data.findIndex(product => product.some(item =>
        item === 'X')
      );
      
      if (parseInt(p.valor) > 0) {
        //console.log("todos los datos: " + that.data)
        that.result = that.data.filter(function (v, i, self) {
          if (v[2] != undefined)
            return self.indexOf(v) > parseInt(p.valor) && self.indexOf(v) < parseInt(p.end) && v[2].length > 1;
        })
        
        that.tempmap = that.result.map(result => {
          let arregloMeses: number[] = [];
          let increseaded = 0;
          for (let i = 0; i <= 11; i++) {
            if (i >= that.nromes) {
              if (result[p.mes + (increseaded)]) {
                arregloMeses[i] = result[p.mes + (increseaded)].toString();
              } else {
                arregloMeses[i] = 0;
              }
              increseaded++;
            } else {
              arregloMeses[i] = 0;
            }
          }

          return {
            customer: result[1],
            project: result[2],
            supplier: result[3],
            mkt: result[4],
            sbu: result[5],
            probability: result[7],
            fcMes: arregloMeses
          };
        });
        if (sheetname == 'OR_List') {
          that.mapped = that.tempmap;
        } else {
          that.mappedSales = that.tempmap;
        }
        
      } else {
        that.myInputVariable.nativeElement.value = '';
        that.FlashMessageService('La hoja no tiene la seccion ORDER FORECAST GPM o SALES FORECAST GPM', 10000, 'danger');
      }
    } else {
      that.myInputVariable.nativeElement.value = '';
      that.FlashMessageService('No se encuentra el mes seleccionado dentro de la hoja', 10000, 'danger');
    }

  };



  readExcel(evt, sheetname, finditem, test) {
    this.buttonText = "Reading XLSX";
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      /* grab first sheet */
      const wsname: string = sheetname;//"OR_List"; //wb.SheetNames[1];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      if (ws) {
        this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));

        test(this.data, this,finditem,sheetname);
        if (sheetname == 'SL_List') {
          this.buttonText = "Save";   
        }
               
      } else {
        this.myInputVariable.nativeElement.value = '';
        this.FlashMessageService('El Archivo no cuenta con la hoja OR o SL de forecast', 10000, 'danger');
      }
    };    
    reader.readAsBinaryString(target.files[0]);
    
  }
}
