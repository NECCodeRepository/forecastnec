export interface RulesTypes {
    value ? : string;
    viewValue ? : string; 
}

export interface RuleType {
    ruletype : string; 
    mktprogram : string;
    description : string;
    months : number;
}

export interface Months {
    value ? : number;
    viewValue ? : string; 
}