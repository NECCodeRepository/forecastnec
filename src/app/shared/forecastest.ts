export interface Forecastest {
    orderId : string;
    ruleTypeID : number;
    customer: string;
    project : string;
    mkt : string;
    amount : number;
    year : number;
    month : number;
    months : object;
    monthValueIndex : number;
    quote : number;
    postpone : number;
    postponeRecomended : number;
 }