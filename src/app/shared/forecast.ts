export interface Forecast {
    forecastName ? : string; // the "?" makes the property optional, 
    dateLoad ? : string;
    year ? : string;
    month ? : string;
    data ? : Object;
    rows ? : number;
    monthNumber ? : number;
    sheet ? : string;
}

export class ForeYearD {
    data ? : Object;
}

export class General {
    data ? : Object;
}


export class ForeMonthD {
    data ? : Object;
}

export class ForecastClass {
    forecastName ? : string; // the "?" makes the property optional, 
    dateLoad ? : string;
    data ? : Object;
}
/*
export class Forecast {
    forecastName : String = "hola";

}
*/