export interface Years {
    value ? : number;
    viewValue ? : number; 
}

export interface MonthsFC {
    value ? : string;
    viewValue ? : string; 
    ant ? : string;
    num ? : number;
}