import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
//import {DataService} from '../../services/data.service';
import {FormControl, Validators} from '@angular/forms';
import { Months } from '../shared/rulesTypes';
import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';


registerLocaleData(es);
@Component({
  selector: 'app-test-dialog',
  templateUrl: './test-dialog.component.html',
  styleUrls: ['./test-dialog.component.css']
})
export class TestDialogComponent implements OnInit {
  months: Months[];
  //postergados: Months[];
  selected : any;
  //selectedP : any;
  //cuota : number;
  desabilitado : boolean = false;
  constructor(public dialogRef: MatDialogRef<TestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any /*, public dataService: DataService*/) { }

formControl = new FormControl('', [
Validators.required
// Validators.email,
]);

getErrorMessage() {
return this.formControl.hasError('required') ? 'Required field' :
this.formControl.hasError('email') ? 'Not a valid email' :
'';
}

ngOnInit() {
  this.selected = this.data.mr;
  //console.log(this.data);
  //console.log(this.selected)
  //this.cuota = this.data.fcMes / this.data.meses;
  this.months = [    
    { value: 1, viewValue: '1 Meses'},
    { value: 2, viewValue: '2 Meses' },
    { value: 3, viewValue: '3 Meses'},
    { value: 6, viewValue: '6 Meses'},
    { value: 10, viewValue: '10 Meses'},
    { value: 12, viewValue: '12 Meses' },
  ];
  /*
  this.postergados = [
    { value: 0, viewValue: '0 Meses'},
    { value: 1, viewValue: '1 Meses'},
    { value: 2, viewValue: '2 Meses' },
    { value: 3, viewValue: '3 Meses'},
  ];
  */
  //this.selectedP = 0;
}

meses(e) {
  //console.log(e);
  this.data.meses = e;
  //this.cuota = this.data.fcMes / e;
}

submit() {
// emppty stuff
}

onNoClick(): void {
this.dialogRef.close();
}

stopEdit(): void {
 //console.log(this.selectedP);
 //this.data.postergar = this.selectedP;
 this.data.accion = "save";
 this.dialogRef.close({data:this.data})
//this.dataService.updateIssue(this.data);
}

delete(): void {
  this.data.accion = "delete";  
  this.dialogRef.close({data:this.data})
}
  
}
