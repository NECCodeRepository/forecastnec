import { Component, OnInit, ViewChild,TemplateRef ,ElementRef} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { MktprogramService } from '../services/mktprogram.service';
import { MktProgram } from '../shared/mktprogram';
import * as $ from 'jquery';
import { MatTableDataSource } from '@angular/material/table';
import { NgFlashMessageService } from 'ng-flash-messages';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mktprogram',
  templateUrl: './mktprogram.component.html',
  styleUrls: ['./mktprogram.component.css']
})
export class MktprogramComponent implements OnInit {
  @ViewChild('fform') registerFormDirective;
  mktProgramForm: FormGroup;
  mktprogram : MktProgram;
  dataSource = new MatTableDataSource();
  mktprograms : MktProgram[];
  errMess: string;
  modalRef: BsModalRef;
  row : Object;
  displayedColumns: string[] = ['mktprogram', 'delete'];
  fullname : string;
  id:string;
  formErrors = {
    //'forecastName': '',
    'mktprogramDesc': ''
  };

  validationMessages = {
    /*
    'forecastName': {
      'required': 'Se necesita un nombre para el forecast'
    },*/
    'mktprogramDesc': {
      'required': 'Se requiere una descripción'
    }
  };

  constructor(private mkt: FormBuilder,
    private mktprogramService: MktprogramService,
    private modalService: BsModalService,
    private ngFlashMessageService: NgFlashMessageService,
    private router: Router, 
    public authService: AuthService) { }

  ngOnInit() {
    this.id = localStorage.getItem('token');
    this.fullname = localStorage.getItem('fullname');
    this.mktProgramForm = this.mkt.group({
      //forecastName: ['', Validators.required],
      mktprogramDesc: ['', Validators.required],

    });
    this.mktProgramForm.valueChanges.subscribe(data => this.onValueChanges(data));

    this.onValueChanges();
    this.refresh();
    
  }

  logout(): void {
    //console.log("Logout");
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  onValueChanges(data?: any) {
    //console.log(data);
    //console.log("entro");

    if (!this.mktProgramForm) { return; }
    const form = this.mktProgramForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        //console.log(field);
        this.formErrors[field] = '';
        const control = form.get(field);
        //console.log(control);
        if (control && control.dirty && !control.valid) {
          //console.log("no es valido");
          const messages = this.validationMessages[field];
          //console.log(messages);
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }

  }

  decline(): void {    
    this.modalRef.hide();
  }

  openModal(template: TemplateRef<any>, element : Object) {    
    this.row = element;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  refresh() {
    this.mktprogramService.getMktPrograms().subscribe(mktprograms => {
      this.mktprograms = mktprograms
      this.dataSource = new MatTableDataSource(mktprograms['data']);
      console.log(mktprograms['data']);
    });
  
  }

  ngAfterViewInit() {
    $.getScript("../../build/js/custom.js", function () {
    });
  }

  deleteRecord() {
    //e.stopPropagation();
    this.mktprogramService.deleteMktPrograms(this.row['_id']).subscribe(forecast => {
      this.refresh();
    });
    this.modalRef.hide();
  }

  accion() {
    this.mktprogram = this.mktProgramForm.value;
    this.mktprogramService.newMktProgram(this.mktprogram).subscribe(result => {
      //console.log(forecast)
      if (result['status'] == 200){
        console.log("status 200");
        this.refresh();
        //this.mkprogramDesc.nativeElement.value = '';
        this.mktProgramForm.reset({
          mktprogramDesc: ['', Validators.required ]
        });
        this.registerFormDirective.resetForm();
        
        
        this.ngFlashMessageService.showFlashMessage({
          messages: ["Registro de FORECAST guardado con exito!"],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'success'
        });
        
      } else {
        console.log("no estatus");
        
        this.ngFlashMessageService.showFlashMessage({
          messages: [result['message']],
          dismissible: true,
          timeout: 3000,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
        
      }
    });
  
}
}
