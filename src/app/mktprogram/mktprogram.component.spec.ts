import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MktprogramComponent } from './mktprogram.component';

describe('MktprogramComponent', () => {
  let component: MktprogramComponent;
  let fixture: ComponentFixture<MktprogramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MktprogramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MktprogramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
