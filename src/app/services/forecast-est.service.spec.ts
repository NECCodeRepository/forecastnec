import { TestBed, inject } from '@angular/core/testing';

import { ForecastEstService } from './forecast-est.service';

describe('ForecastEstService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ForecastEstService]
    });
  });

  it('should be created', inject([ForecastEstService], (service: ForecastEstService) => {
    expect(service).toBeTruthy();
  }));
});
