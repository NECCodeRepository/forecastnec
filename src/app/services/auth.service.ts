import { Injectable } from '@angular/core';
import { Auth, AuthClass} from '../shared/auth';

import { delay,map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
/* rest angular */
import { Restangular } from 'ngx-restangular';
//Handle error
import { ProcessHTTPMsgService } from './process-httpmsg.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http : HttpClient,private proccessHTTPMsgService : ProcessHTTPMsgService, 
    private restangular : Restangular) { }

  getAuth(username,password): Observable<Auth[]> {
     return this.restangular.all('auth/user/'+username+'/'+password).customGET("");
  }

  logout(): void {
    localStorage.setItem('isLoggedIn',"false");
    localStorage.removeItem('token');
    localStorage.setItem('fullname','');
    localStorage.removeItem('fullname');
  }
}
