import { Injectable } from '@angular/core';
import { MktProgram } from '../shared/mktprogram';
import { RuleType } from '../shared/rulesTypes';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
/* rest angular */
import { Restangular } from 'ngx-restangular';
//Handle error
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class RulestypesService {

  constructor(private http : HttpClient,private proccessHTTPMsgService : ProcessHTTPMsgService, 
    private restangular : Restangular) { }

  getMktPrograms(): Observable<MktProgram[]> {
    return this.restangular.all('mktprograms').customGET("");
   }

   newRuleType(ruletype: RuleType) {
     console.log("llego2");
     console.log(ruletype);
    return this.restangular.all('ruletype').post(ruletype);
  }

  getRulesTypes(): Observable<RuleType[]> {
    //return of(PROMOTIONS).pipe(delay(2000));
    return this.restangular.all('rulestypes').customGET("");
    //return this.restangular.all('mktprograms').getList();
  }

  deleteRuleType(id : String): Observable<RuleType[]> {
    //this.restangular.remove('api/test/'+id);
    return this.restangular.one('ruletype/remove/',id).remove();
    //return this.restangular.all('api/test').customGET("");
  }

}
