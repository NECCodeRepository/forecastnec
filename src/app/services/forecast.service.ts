import { Injectable } from '@angular/core';

import { Forecast, ForecastClass,ForeYearD, ForeMonthD,General } from '../shared/forecast';

import { delay,map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
/* rest angular */
import { Restangular } from 'ngx-restangular';
//Handle error
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {

  constructor(private http : HttpClient,private proccessHTTPMsgService : ProcessHTTPMsgService, 
    private restangular : Restangular) { }

    submitForecast(forecast: Forecast) {
      return this.restangular.all('fore').post(forecast);
    }
    /*
    getForecasts(): Observable<ForecastClass[]> {
      return this.restangular.all('forecasts').getList();
    }
    */

   getForeYearD(): Observable<ForeYearD[]> {
    return this.restangular.all('foreyeard').customGET("");
   }

   getDataAnalysis(ano,mes,anob,mesb,vista,mesforview): Observable<General[]> {
     console.log(anob);
     console.log(mesb)
    return this.restangular.all('forecasts/'+ano+'/'+mes+'/'+anob+'/'+mesb+'/'+vista+'/'+mesforview).customGET("");
   }

   getDataOneForecast(ano,mes): Observable<Forecast[]> {
   return this.restangular.all('forecasts/'+ano+'/'+mes).customGET("");
  }

   getForeMonthD(value): Observable<ForeMonthD[]> {
    return this.restangular.all('foremonthd/'+value).customGET("");
   }

   getForecasts(): Observable<Forecast[]> {
     return this.restangular.all('forecasts').customGET("");
   }

   deleteForecasts(id : String): Observable<Forecast[]> {
    //this.restangular.remove('api/test/'+id);
    return this.restangular.one('fore/remove/',id).remove();
    //return this.restangular.all('api/test').customGET("");
  }
  deleteForecastsParams(year : String, month: String): Observable<Forecast[]> {
    //this.restangular.remove('api/test/'+id);
    return this.restangular.one('fore/remove/'+year+'/',month).remove();
    //return this.restangular.all('api/test').customGET("");
  }
}
