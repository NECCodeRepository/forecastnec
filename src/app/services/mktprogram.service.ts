import { MktProgram } from '../shared/mktprogram';

import { Injectable } from '@angular/core';
import { delay,map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
/* rest angular */
import { Restangular } from 'ngx-restangular';
//Handle error
import { ProcessHTTPMsgService } from './process-httpmsg.service';
@Injectable({
  providedIn: 'root'
})
export class MktprogramService {

  constructor(private http : HttpClient,private proccessHTTPMsgService : ProcessHTTPMsgService, 
    private restangular : Restangular) { }

    newMktProgram(mktprogram: MktProgram) {
      return this.restangular.all('mktprogram').post(mktprogram);
    }

    getMktPrograms(): Observable<MktProgram[]> {
      //return of(PROMOTIONS).pipe(delay(2000));
      return this.restangular.all('mktprograms').customGET("");
      //return this.restangular.all('mktprograms').getList();
    }

    deleteMktPrograms(id : String): Observable<MktProgram[]> {
      //this.restangular.remove('api/test/'+id);
      return this.restangular.one('mktprogram/remove/',id).remove();
      //return this.restangular.all('api/test').customGET("");
    }
 

}
