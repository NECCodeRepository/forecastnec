import { Injectable } from '@angular/core';
import { Forecastest } from '../shared/forecastest';
import { HttpClient } from '@angular/common/http';
import { Restangular } from 'ngx-restangular';
import { Observable, of } from 'rxjs';
import { ProcessHTTPMsgService } from './process-httpmsg.service';


@Injectable({
  providedIn: 'root'
})
export class ForecastEstService {

  constructor(private http : HttpClient,private proccessHTTPMsgService : ProcessHTTPMsgService, 
    private restangular : Restangular) { }

    newForecastest(fe: any) {
      return this.restangular.all('forecastest').post(fe);
    }

    updateForecastest(fe: any,meses:number,id,indice) {
      return this.restangular.all('forecastest/update/'+id).customPUT({fe:fe,meses:meses,indice:indice});
    }

    getDataForecastest(id): Observable<Forecastest[]> {
      return this.restangular.all('forecastest/'+id).customGET("");
    }

    deleteForecaststest(orderId : String): Observable<Forecastest[]> {      
      return this.restangular.one('forecastest/remove/',orderId).remove();      
    }

    deleteForecaststestOne(id : String): Observable<Forecastest[]> {      
      return this.restangular.one('forecastest/removeone/',id).remove();      
    }
    

  
}
