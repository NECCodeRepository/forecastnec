import { TestBed, inject } from '@angular/core/testing';

import { RulestypesService } from './rulestypes.service';

describe('RulestypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RulestypesService]
    });
  });

  it('should be created', inject([RulestypesService], (service: RulestypesService) => {
    expect(service).toBeTruthy();
  }));
});
