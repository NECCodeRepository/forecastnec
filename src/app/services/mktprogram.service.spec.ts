import { TestBed, inject } from '@angular/core/testing';

import { MktprogramService } from './mktprogram.service';

describe('MktprogramService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MktprogramService]
    });
  });

  it('should be created', inject([MktprogramService], (service: MktprogramService) => {
    expect(service).toBeTruthy();
  }));
});
