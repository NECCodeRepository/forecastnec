import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { baseURL } from './shared/baseurl';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';

import { FlexLayoutModule } from '@angular/flex-layout';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing/app-routing.module';
import {MatExpansionModule} from '@angular/material/expansion';

import { FileDropModule } from 'ngx-file-drop';

import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule,
  MatToolbarModule, MatMenuModule, MatIconModule, MatProgressSpinnerModule,MatSortModule
} from '@angular/material';

import { MatCheckboxModule } from '@angular/material/checkbox';

import {MatSelectModule} from '@angular/material/select';

import { RestangularModule, Restangular } from 'ngx-restangular';
import { RestangularConfigFactory } from './shared/restConfig';

/* reactive module*/
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { DataComponent } from './data/data.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { SidemenuBottonComponent } from './sidemenu-botton/sidemenu-botton.component';
import { AuthGuard } from './auth.guard';
import { NgFlashMessagesModule } from 'ng-flash-messages';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RulesComponent } from './rules/rules.component';
import { MktprogramComponent } from './mktprogram/mktprogram.component';
import { SalesComponent } from './sales/sales.component';
import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { TestDialogComponent } from './test-dialog/test-dialog.component';



import { AddDialogComponentComponent } from './add-dialog-component/add-dialog-component.component'

registerLocaleData(es);


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    DataComponent,
    SidemenuComponent,
    SidemenuBottonComponent,
    RulesComponent,
    MktprogramComponent,
    SalesComponent,
    TestDialogComponent,    
    AddDialogComponentComponent
  ],
  imports: [
    DragDropModule,
    MatSortModule,
    AppRoutingModule,
    MatSelectModule,
    MatSlideToggleModule, 
    MatExpansionModule,   
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    FileDropModule,
    MatProgressSpinnerModule,
    ModalModule.forRoot(),
    NgFlashMessagesModule.forRoot(),
    RestangularModule.forRoot(RestangularConfigFactory)
  ],
  entryComponents: [
    TestDialogComponent,AddDialogComponentComponent
],
  providers: [AuthGuard,{provide: 'BaseURL', useValue: baseURL}],
  bootstrap: [AppComponent]
})
export class AppModule { }
