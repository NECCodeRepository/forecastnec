import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import * as $ from 'jquery';
import { ForecastService } from '../services/forecast.service';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { NgFlashMessageService } from 'ng-flash-messages';
import { MonthsFC } from '../shared/years';
import { MatTableDataSource } from '@angular/material/table';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RulestypesService } from '../services/rulestypes.service';
import { RuleType } from '../shared/rulesTypes';

import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { TestDialogComponent } from '../test-dialog/test-dialog.component';
import { AddDialogComponentComponent } from '../add-dialog-component/add-dialog-component.component';
import { Forecast } from '../shared/forecast';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

// test
//import { Forecastest } from '../shared/forecastest';
import { ForecastEstService } from '../services/forecast-est.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {
  @ViewChild('fform') registerFormDirective;
  years: string[];
  forecastPredForm: FormGroup;
  selected: any;
  modalRef: BsModalRef;
  row: Object;
  datarules: RuleType[];
  selectedMonth: any;
  forecast: Forecast[];
  objeto: any;
  dataSource = new MatTableDataSource();
  months: string[];
  calculoMes: number;
  monthsFC: MonthsFC[];
  resultado: number;
  mesanterior: number;
  totalMonth: number = 0;
  totalFC: number = 0;
  i: number = 1;
  arreglo = [];
  arreglodet = {};
  showTable: boolean = true;
  showResult: boolean = false;
  fullname: string;
  buttonGenerate: boolean = false;
  ButtonResetear: boolean = true;
  id: string;
  orderData: any;
  columns: Array<any> = [
    { name: 'project', label: 'PROJECT' },
    { name: 'mkt', label: 'mkt' },
    { name: 'postponeRecomended', label: 'postponeRecomended' }
  ];
  columnsAdd: Array<any>;
  displayedColumns: string[] = this.columns.map(column => column.name);
  //displayedColumns: string[] = ['project', 'mkt'/*,'fcMes','meses','mes1','mes2','mes3','mes4','mes5','mes6','mes7','mes8','mes9','mes10','mes11','mes12','star'*/];
  //displayedColumns: string[] = ['forecastName', 'month'];
  rulestypes: RuleType[];


  constructor(
    private forecastService: ForecastService,
    private rulestypesService: RulestypesService,
    private fb: FormBuilder,
    private modalService: BsModalService,
    public dialog: MatDialog,
    private router: Router,
    public authService: AuthService,
    // test service
    private forecastEstService: ForecastEstService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.id = localStorage.getItem('token');
    this.fullname = localStorage.getItem('fullname');
    this.forecastService.getForeYearD().subscribe(years => {
      this.years = years['data'];
    });

    this.monthsFC = [
      { value: "FC_NOV", viewValue: "NOVIEMBRE", ant: "11", num: 8 },
      { value: "FC_DEC", viewValue: "DICIEMBRE", ant: "12", num: 9 },
      { value: "FC_JAN", viewValue: "ENERO", ant: "1", num: 10 },
      { value: "FC_FEB", viewValue: "FEBRERO", ant: "2", num: 11 },
      { value: "FC_MAR", viewValue: "MARZO", ant: "3", num: 12 },
      { value: "FC_APR", viewValue: "ABRIL", ant: "4", num: 1 },
      { value: "FC_MAY", viewValue: "MAYO", ant: "5", num: 2 },
      { value: "FC_JUN", viewValue: "JUNIO", ant: "6", num: 3 },
      { value: "FC_JUL", viewValue: "JULIO", ant: "7", num: 4 },
      { value: "FC_AGO", viewValue: "AGOSTO", ant: "8", num: 5 },
      { value: "FC_SEP", viewValue: "SEPTIEMBRE", ant: "9", num: 6 },
      { value: "FC_OCT", viewValue: "OCTUBRE", ant: "10", num: 7 }
    ];

    this.rulestypesService.getRulesTypes().subscribe(rulestypes => {
      this.rulestypes = rulestypes['data'];
    });
  }

  logout(): void {
    //console.log("Logout");
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  ngAfterViewInit() {
    $.getScript("../../build/js/custom.js", function () {
    });
  }

  createForm(): void {
    this.forecastPredForm = this.fb.group({
      selectYears: [this.selected, Validators.required],
      selectMonths: [this.selectedMonth, Validators.required]
    });
  }

  yearChange(e) {
    this.forecastService.getForeMonthD(e).subscribe(months => {
      this.months = months['data'];
    });
    this.selected = e;
  }

  isOdd(element) {
    return +element > 0;
  }

  refresh() {
    //this.displayedColumns = this.columns.map(column => column.name);
    this.columnsAdd = [];
    this.columnsAdd.push(this.columns[0]);
    this.columnsAdd.push(this.columns[1]);
    this.columnsAdd.push(this.columns[2]);
    let nextMonth: number;
    this.totalFC = 0;
    this.totalMonth = 0;
    this.forecastService.getDataOneForecast(this.selected, this.selectedMonth).subscribe(data => {
      this.orderData = data;
      this.forecastEstService.getDataForecastest(this.orderData['data']['_id']).subscribe(dataest => {
        nextMonth = this.monthsFC[this.monthsFC.findIndex(p => p.value == this.selectedMonth)].num + 1;
        //if (nextMonth > 11)
        //nextMonth = 12;

        if (dataest['data'].length > 0) {
          //console.log("aqui")
          this.showTable = false;
          this.showResult = true;
          this.buttonGenerate = true;
          this.ButtonResetear = false;
        } else {
          this.showTable = true;
          this.showResult = false;
          this.buttonGenerate = false;
          this.ButtonResetear = true;
        }
        this.objeto = dataest;
        console.log(dataest['data']);
        // relaciono mattable con los datos obtenidos
        this.dataSource = new MatTableDataSource(dataest['data']);
        // Defino mes minimo 11 para determinar los meses a mostrar en la tabla
        let minimo: number = 11;
        // Recorro todos los elementos de la consulta forecastest
        dataest['data'].forEach(function (element) {
          if (minimo > element.months.findIndex(this.isOdd)) {
            minimo = element.months.findIndex(this.isOdd);
          }
          this.totalFC += element.months.reduce((res, value) => +res + +value, 0);

          this.totalMonth += element.months.reduce(function (res, value, index) {
            if (index == nextMonth - 1) {
              //console.log(+res + +value);
              return +res + +value;
            } else {
              return +res + +0;
            }
          }, 0);

          //console.log(this.totalMonth)

        }.bind(this));
        //console.log("minimo es:" + minimo)
        dataest['data'].forEach(function (element) {
          // filtro el arreglo months para mostrar solamente del indice obtenido en adelante
          let res = element.months.filter(function (element, index) {
            return index >= minimo;
          });
          // recorro el arreglo monthsFC y obtengo el nombre de cada MES para añadirlo al objeto principal element
          // junto a su valor que corresponde al mes.
          let cont = 0;
          let valuecont = '0';
          for (let i = minimo; i < 12; i++) {
            if (res[cont])
              valuecont = res[cont];
            else
              valuecont = '0';
            element[this.monthsFC[this.monthsFC.findIndex(p => p.num == i + 1)].value] = valuecont;
            cont++;
          }

        }.bind(this));
        // añado las columnas que corresponden al displayedcolumns
        for (let i = minimo; i < 12; i++) {
          this.columnsAdd.push({ name: this.monthsFC[this.monthsFC.findIndex(p => p.num == i + 1)].value, label: 'ok' });
          //this.columns.push({name:this.monthsFC[this.monthsFC.findIndex(p => p.num == i+1)].value,label:'ok'});          
        }
        //this.columns.push({name:'star', label:'edit'});
        this.columnsAdd.push({ name: 'star', label: 'edit' });

        this.displayedColumns = this.columnsAdd.map(column => column.name);
        console.log(this.displayedColumns);

      });

    });
  }

  monthChange(e) {
    this.refresh();
  }

  openModal(template: TemplateRef<any>, element: Object) {
    this.row = element;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }



  reset() {
    if (this.orderData['data']['_id']) {
      //console.log("tengo el id, puedo borrar y llamar refresh");
      this.forecastEstService.deleteForecaststest(this.orderData['data']['_id']).subscribe(fe => {
        //console.log("fe");
        //console.log(fe)
        this.refresh();
      });
    }
  }

  rPR( project, mkt) {
    let retorno: number = 1;
    let description : boolean = false;
    let mktdescription: boolean = false;
    this.rulestypes.forEach(function(element) {
      
      if (mkt && description == false && mktdescription == false) {

        if (element.mktprogram.toLowerCase().trim() == mkt.toLowerCase().trim() && element.description.toLowerCase().trim() == "n/a"){          
          retorno = element.months;              
        } 
        if (element.description.toLowerCase().trim() != "n/a") {
          if (project.toLowerCase().trim().indexOf(element.description.toLowerCase().trim())  != -1 && mktdescription == false) {            
            retorno = element.months;            
            description = true;
          }
          if (element.mktprogram.toLowerCase().trim() == mkt.toLowerCase().trim() && project.toLowerCase().trim().indexOf(element.description.toLowerCase().trim())  != -1){            
            mktdescription = true;
            retorno = element.months;    
                 
          } 

          
        }
      }

    });
    console.log("el retorno es:" + retorno);
    return retorno;
  }

  generate() {
    let mesNum: number = this.monthsFC[this.monthsFC.findIndex(p => p.value == this.selectedMonth)].num;
    this.rulestypesService.getRulesTypes().subscribe(rules => {
      //console.log(rules);
      this.datarules = rules['data'];
      //console.log(this.datarules.length)

      this.arreglo = [];
      for (var x in this.orderData['data']['data']) {
        this.arreglodet = {
          orderId: this.orderData['data']['_id'],
          ruleTypeID: '0',
          customer: this.orderData['data']['data'][x]['customer'],
          project: this.orderData['data']['data'][x]['project'],
          mkt: this.orderData['data']['data'][x]['mkt'],
          amount: +this.orderData['data']['data'][x]['fcMes'].reduce((a, b) => a + b, 0),
          year: +this.selected,
          month: +mesNum,
          months: this.orderData['data']['data'][x]['fcMes'],
          monthValueIndex: 0,
          quote: 0,
          postpone: 0,
          postponeRecomended: this.rPR(this.orderData['data']['data'][x]['project'], this.orderData['data']['data'][x]['mkt'])
        }
        this.arreglo.push(this.arreglodet);
      }
      if (this.arreglo.length > 0) {
        this.forecastEstService.newForecastest(this.arreglo).subscribe(fe => {
          //console.log(fe)
          this.refresh();
        });
      } else {
        console.log("esta vacio");
      }
    });

  }

  addNew() {

    const dialogRef = this.dialog.open(AddDialogComponentComponent, {
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {

        this.arreglo = [];
        let months = [];
        for (let y = 0; y <= 11; y++) {
          if (y == +result['data'].meses - 1) {
            months.push(+result['data'].amount);
          } else {
            months.push(0);
          }

        }

        this.arreglodet = {
          orderId: this.orderData['data']['_id'],
          ruleTypeID: '0',
          customer: result['data'].customer,
          project: result['data'].project,
          mkt: result['data'].mkt,
          amount: +result['data'].amount,
          year: +this.selected,
          month: this.monthsFC[this.monthsFC.findIndex(p => p.value == this.selectedMonth)].num,
          months: months,
          monthValueIndex: +result['data'].meses,
          quote: 0,
          postpone: 0,
          postponeRecomended: this.rPR(result['data'].project, result['data'].mkt)
        }
        this.arreglo.push(this.arreglodet);
        if (this.arreglo.length > 0) {
          this.forecastEstService.newForecastest(this.arreglo).subscribe(fe => {
            //console.log(fe)
            this.refresh();
          });
        }

        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        //this.exampleDatabase.dataChange.value.push(this.dataService.getDialogData());
        //this.refreshTable();
      }
    });

  }

  startEdit(id: number, project: string, mkt: string, realId: string,mr:number/*i: number, id: number, title: string, state: string, url: string, created_at: string, updated_at: string*/) {
    //this.id = id;
    // index row is used just for debugging proposes and can be removed
    //this.index = i;
    //id, title: title, state: state, url: url, created_at: created_at, updated_at: updated_at
    const dialogRef = this.dialog.open(TestDialogComponent, {
      data: { project: project, mkt: mkt, id: realId, mr:mr }
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        console.log(result)
        if (this.objeto['data'] && result['data'].mr) {
          this.objeto['data'].forEach(function (element) {
            if (element._id == result['data'].id) {
              console.log(this.objeto['data'])
              let indice = element.months.findIndex(this.isOdd);
              let valor = element.months[indice];
              if (+element.monthValueIndex != 0) {
                indice = element.monthValueIndex;
              }
              element.months[indice] = 0;
              if (indice + result['data'].mr > 11) {
                element.months[11] = valor;
              } else {
                element.months[+indice + +result['data'].mr] = valor;
              }
              //console.log("indice:" + indice);

              this.forecastEstService.updateForecastest(element.months, result['data'].mr, result['data'].id, indice).subscribe(fe => {
                this.refresh();
              });


            }
          }.bind(this));
        }
        if (result['data'].accion == "delete") {
          console.log("entro al delete");
          this.forecastEstService.deleteForecaststestOne(result['data'].id).subscribe(fe => {
            this.refresh();
          });
        }
      }

    });

    //this.dialog.open(TestDialogComponent, {width: '500px', height: '450px'});

  }



}
