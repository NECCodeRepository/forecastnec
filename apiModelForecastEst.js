var mongoose = require('mongoose');

var forecastEstSchema = mongoose.Schema({    
        orderId : {type:String, required:true},
        ruleTypeID : {type:String, require:true},
        customer : {type:String, require},
        project : {type:String, required:true},
        mkt : {type:String, required:false},
        amount: {type:Number,required:true},
        year : {type:Number,required:true},
        month : {type:Number,required:true},
        months : {type:Object},
        monthValueIndex : {type:Number,required:true},
        quote : {type:Number},
        postpone : {type:Number},        
        postponeRecomended : {type:Number},
});

forecastEstSchema.index({orderId:1,project: 1, mkt: 1,amount:1,month:1}, {unique: true});



var ForeEst = module.exports = mongoose.model('forecastest', forecastEstSchema);


module.exports.get = function (callback, limit) {
    ForeEst.find(callback).limit(limit);    
    
}

