var mongoose = require('mongoose');
// Setup schema
var forecastSchema = mongoose.Schema({    
    forecastName : {type:String, required:true},
    dateLoad: {type:String, required:true},
    year : {type:String,required:true},
    month : {type:String, required:true},
    monthNumber : {type:Number, required:true},
    data : Object,
    rows: {type:Number, required:true},
    sheet : {type:String, required:true},
});
forecastSchema.index({year: 1, month: 1,sheet: 1}, {unique: true});
var Fore = module.exports = mongoose.model('forecast',forecastSchema)

module.exports.get = function (callback, limit) {
    //Auth.find(callback).limit(limit);
    Fore.find(callback).limit(limit);
}

