var mongoose = require('mongoose');

var authSchema = mongoose.Schema({    
        username : {type:String, required:true},
        fullname : {type:String, required:true},
        password : {type:String, required:true}    
});



authSchema.index({username: 1, password: 1}, {unique: true});



var Auth = module.exports = mongoose.model('auth', authSchema);


module.exports.get = function (callback, limit) {
    Auth.find(callback).limit(limit);    
    
}

