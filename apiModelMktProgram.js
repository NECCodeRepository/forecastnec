var mongoose = require('mongoose');
// Setup schema
var mktprogramSchema = mongoose.Schema({
    
        mktprogram : {type:String, required:true}
    
});
mktprogramSchema.index({mktprogram: 1}, {unique: true});
// Export test model
var MktP = module.exports = mongoose.model('mktprogram', mktprogramSchema);

module.exports.get = function (callback, limit) {
    MktP.find(callback).limit(limit);    
}

