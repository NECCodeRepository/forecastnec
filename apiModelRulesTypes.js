var mongoose = require('mongoose');

var rulesSchema = mongoose.Schema({
    
    ruletype : {type:String, required:true},
    mktprogram: {type:String, required:true, default: 'n/a'},
    description : {type:String,required:true,default:'n/a'},
    months : {type:Number, required:true}

});
rulesSchema.index({ruletype: 1, mktprogram: 1,description:1}, {unique: true});
var Rules = module.exports = mongoose.model('rulestype',rulesSchema)

module.exports.get = function (callback, limit) {
    Rules.find(callback).limit(limit);
}

