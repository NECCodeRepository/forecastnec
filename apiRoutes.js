let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
       status: 'API Its Working',
       message: 'Welcome to RESTHub crafted with love!',
    });
});
// Import contact controller
var testController = require('./apiController');
// Contact routes
router.route('/auth')
    .get(testController.index)
    .post(testController.new);
router.route('/auth/:username')
    .get(testController.view)
    .delete(testController.delete);
router.route('/auth/user/:username/:password')
    .get(testController.auth);

// obtiene todos los forecasts
router.route('/forecasts')    
    .get(testController.indexData);
//obtiene forecast especifico
router.route('/forecasts/:ano/:mes/:anob/:mesb/:view/:mesforview')    
    .get(testController.indexDataId);
router.route('/forecasts/:ano/:mes')    
    .get(testController.indexDataIdS);
//inserta nuevo forecast

router.route('/fore')    
    .post(testController.newData);
// elimina forecast
router.route('/fore/remove/:id')    
    .delete(testController.deleteData);
router.route('/fore/remove/:year/:month')    
    .delete(testController.deleteDataParams);
router.route('/foreyeard')
    .get(testController.foreyeard);
router.route('/foremonthd/:year')
    .get(testController.foremonthd);

router.route('/mktprogram')    
    .post(testController.newMktProgram);
router.route('/mktprograms')    
    .get(testController.indexMktProgram);
router.route('/mktprogram/remove/:id')    
    .delete(testController.deleteMktProgram);

router.route('/ruletype')    
    .post(testController.newRuleType);
router.route('/rulestypes')    
    .get(testController.indexRuleType);
router.route('/ruletype/remove/:id')    
    .delete(testController.deleteRuleType);

// test routes
router.route('/test')    
    .post(testController.newTest);

// forecast Estimacion y obtencion de la base para procesamiento
router.route('/forecastest').post(testController.newForecastest);
router.route('/forecastest/:id/').get(testController.indexForecastest);
router.route('/forecastest/update/:id/').put(testController.updateForecastest);
router.route('/forecastest/remove/:orderId').delete(testController.deleteForecastest);
router.route('/forecastest/removeone/:id').delete(testController.deleteForecastestUnique);


// Export API routes
module.exports = router;